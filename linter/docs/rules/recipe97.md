#  ``keyof`` operator is not supported

Rule ``arkts-no-keyof``

**Severity: error**

ArkTS has no `keyof` operator because the object layout is defined
at compile time, and cannot be changed at runtime. Object fields can only be
accessed directly.


## TypeScript


```

    class Point {
        x: number = 1
        y: number = 2
    }

    type PointKeys = keyof Point  // The type of PointKeys is "x" | "y"

    function getPropertyValue(obj: Point, key: PointKeys) {
        return obj[key]
    }

    let obj = new Point()
    console.log(getPropertyValue(obj, "x"))  // prints "1"
    console.log(getPropertyValue(obj, "y"))  // prints "2"

```

## ArkTS


```

    class Point {
        x: number = 1
        y: number = 2
    }

    function getPropertyValue(obj: Point, key: string): number {
        if (key == "x") {
            return obj.x
        }
        if (key == "y") {
            return obj.y
        }
        throw new Error()  // No such property
    }

    function main(): void {
        let obj = new Point()
        console.log(getPropertyValue(obj, "x"))  // prints "1"
        console.log(getPropertyValue(obj, "y"))  // prints "2"
    }

```


