let person = {name: "Bob", isEmployee: true}

let n = person["name"]
let e = person["isEmployee"]
let s = person["office"]


class Person {
    constructor(name: string, isEmployee: boolean) {
        this.name = name
        this.isEmployee = isEmployee
    }

    name: string
    isEmployee: boolean
}

let person2 = new Person("Bob", true)
let n1 = person2.name
let e1 = person2.isEmployee