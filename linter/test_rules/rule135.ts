var C = (function() {
    function C(n: number) {
        this.p = n // Compile-time error only with noImplicitThis
    }
    C.staticProperty = 0
    return C
})()
C.staticProperty = 1