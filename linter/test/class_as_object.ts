/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Something, SomethingFactory, SomethingBar } from "./oh_modules/ohos_factory";

class C {
    static a = 5
    static b = 8
}

namespace H {
    export class G {}
}

let c = C
c = C
let g = H.G
g = H.G

let ca = C.a
let cb = C.b

let cc = new C()
let gg = new H.G()

function foo1(arg: typeof C) {}
foo1(C)

function foo2(arg: C) {
    return C
}

function foo3(arg: typeof H.G) {}
foo3(H.G)

function foo4(arg: H.G) {
    return H.G
}

class A {}
interface B {}
interface I {}
class CC extends A implements I, B {}

class D {
    constructor(arg: typeof C) {}
}
new D(C)

type X = D;

namespace test1 {
    class SomethingFoo extends Something { }
  
    let x = SomethingFactory.getInstance().create(SomethingFoo).beep();
    let y = SomethingFactory.getInstance().create(SomethingBar).beep();
}
