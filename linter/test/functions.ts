/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function restSpread() {
  const arr = [1, 2, 3];
  function test(a, ...t) {
    console.log(a); // 1
    console.log(t[0]); // 2
    console.log(t[1]); // 3
  }
  test(1, ...arr);
}

class MyGenerator {
  public *getValues() {
    // you can put the return type Generator<number>, but it is ot necessary as ts will infer
    let index = 1;
    while (true) {
      yield index;
      index = index + 1;

      if (index > 10) {
        break;
      }
    }
  }
}

function defaultTypeParam<t, tt = string>(i: t, j: tt) {
  const c = i;
  const s = j;
}

function functionExpressionTest(): void {
  const empty = function () {};

  const multiply = function (x: number, y): number {
    return x * y;
  };

  function createFunc(): () => number {
    return function () {
      return 100;
    };
  }

  const foobar = (function () {
    return 'get result immediately';
  })();

  (function () {
    console.log('foo!');
  })();

  void (function () {
    console.log('bar!');
  })();

  const factorial = function func(n: number): number {
    return n === 1 ? 1 : n * func(n - 1);
  };

  const array = [1, 2, 3, 4, 5, 6];
  const double = array.map(function (e) {
    return e * 2;
  });
  const even = array.filter(function (x) {
    return x % 2 === 0;
  });

  const generic = function <T, E>(t: T, e: E) {
    return t;
  };
}

function arrowFunctionTest() {
  const empty = () => {}; // no return type

  const double = (x: number) => x * 2; // no return type

  const square = (x): number => x * x; // no param type

  const sqrt = (x) => Math.sqrt(x); // shortcut syntax
  const even = [1, 2, 3, 4, 5, 6].filter((x) => x % 2 === 0); // shortcut syntax

  const foo = (x: number, y): boolean => x == y; // types are partly omitted

  const generic = <T, E>(t: T, e: E) => t; // Generic lambda
}

function fooThis(i: number): void {
  this.c = 10;
}
class C {
  c: number;
  m = fooThis;
}

function choose<T>(x: T, y: T): T {
  return Math.random() < 0.5 ? x : y;
}
const choice1 = choose(10, 20);
const choice2 = choose<string>('apple', 'orange');

class Collection<T> {
  items: T[] = [];

  constructor(...args: T[]) {
    if (!args) return;

    for (const arg of args) this.items.push(arg);
  }
}
const col = new Collection<number>(1, 2, 3);
const col2 = new Collection('a', 'b', 'c');
