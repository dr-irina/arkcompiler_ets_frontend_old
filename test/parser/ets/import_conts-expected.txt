{
  "type": "Program",
  "statements": [
    {
      "type": "ImportDeclaration",
      "source": {
        "type": "StringLiteral",
        "value": "std/math/consts",
        "loc": {
          "start": {
            "line": 1,
            "column": 65
          },
          "end": {
            "line": 1,
            "column": 82
          }
        }
      },
      "specifiers": [
        {
          "type": "ImportSpecifier",
          "local": {
            "type": "Identifier",
            "name": "doubleNaN",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 9
              },
              "end": {
                "line": 1,
                "column": 18
              }
            }
          },
          "imported": {
            "type": "Identifier",
            "name": "doubleNaN",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 9
              },
              "end": {
                "line": 1,
                "column": 18
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 9
            },
            "end": {
              "line": 1,
              "column": 18
            }
          }
        },
        {
          "type": "ImportSpecifier",
          "local": {
            "type": "Identifier",
            "name": "doubleInf",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 20
              },
              "end": {
                "line": 1,
                "column": 29
              }
            }
          },
          "imported": {
            "type": "Identifier",
            "name": "doubleInf",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 20
              },
              "end": {
                "line": 1,
                "column": 29
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 20
            },
            "end": {
              "line": 1,
              "column": 29
            }
          }
        },
        {
          "type": "ImportSpecifier",
          "local": {
            "type": "Identifier",
            "name": "doubleNegInf",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 31
              },
              "end": {
                "line": 1,
                "column": 43
              }
            }
          },
          "imported": {
            "type": "Identifier",
            "name": "doubleNegInf",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 31
              },
              "end": {
                "line": 1,
                "column": 43
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 31
            },
            "end": {
              "line": 1,
              "column": 43
            }
          }
        },
        {
          "type": "ImportSpecifier",
          "local": {
            "type": "Identifier",
            "name": "doubleEpsilon",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 45
              },
              "end": {
                "line": 1,
                "column": 58
              }
            }
          },
          "imported": {
            "type": "Identifier",
            "name": "doubleEpsilon",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 45
              },
              "end": {
                "line": 1,
                "column": 58
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 45
            },
            "end": {
              "line": 1,
              "column": 58
            }
          }
        }
      ],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 83
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "absTest1",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 11
                },
                "end": {
                  "line": 2,
                  "column": 19
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "absTest1",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 11
                    },
                    "end": {
                      "line": 2,
                      "column": 19
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 24
                    },
                    "end": {
                      "line": 2,
                      "column": 30
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "v",
                            "typeAnnotation": {
                              "type": "ETSPrimitiveType",
                              "loc": {
                                "start": {
                                  "line": 3,
                                  "column": 12
                                },
                                "end": {
                                  "line": 3,
                                  "column": 18
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 8
                              },
                              "end": {
                                "line": 3,
                                "column": 9
                              }
                            }
                          },
                          "init": {
                            "type": "NumberLiteral",
                            "value": 0,
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 21
                              },
                              "end": {
                                "line": 3,
                                "column": 24
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 3,
                              "column": 8
                            },
                            "end": {
                              "line": 3,
                              "column": 24
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 3,
                          "column": 4
                        },
                        "end": {
                          "line": 3,
                          "column": 24
                        }
                      }
                    },
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "actual",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 4,
                                "column": 8
                              },
                              "end": {
                                "line": 4,
                                "column": 14
                              }
                            }
                          },
                          "init": {
                            "type": "CallExpression",
                            "callee": {
                              "type": "Identifier",
                              "name": "abs",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 4,
                                  "column": 17
                                },
                                "end": {
                                  "line": 4,
                                  "column": 20
                                }
                              }
                            },
                            "arguments": [
                              {
                                "type": "Identifier",
                                "name": "v",
                                "decorators": [],
                                "loc": {
                                  "start": {
                                    "line": 4,
                                    "column": 21
                                  },
                                  "end": {
                                    "line": 4,
                                    "column": 22
                                  }
                                }
                              }
                            ],
                            "optional": false,
                            "loc": {
                              "start": {
                                "line": 4,
                                "column": 17
                              },
                              "end": {
                                "line": 4,
                                "column": 23
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 4,
                              "column": 8
                            },
                            "end": {
                              "line": 4,
                              "column": 23
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 4,
                          "column": 4
                        },
                        "end": {
                          "line": 4,
                          "column": 24
                        }
                      }
                    },
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "Identifier",
                        "name": "actual",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 5,
                            "column": 11
                          },
                          "end": {
                            "line": 5,
                            "column": 17
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 4
                        },
                        "end": {
                          "line": 5,
                          "column": 17
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 31
                    },
                    "end": {
                      "line": 6,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 19
                  },
                  "end": {
                    "line": 6,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 19
                },
                "end": {
                  "line": 6,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 1
              },
              "end": {
                "line": 6,
                "column": 2
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 9,
                  "column": 10
                },
                "end": {
                  "line": 9,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 9,
                      "column": 10
                    },
                    "end": {
                      "line": 9,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 9,
                      "column": 18
                    },
                    "end": {
                      "line": 9,
                      "column": 21
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "NumberLiteral",
                        "value": 0,
                        "loc": {
                          "start": {
                            "line": 10,
                            "column": 11
                          },
                          "end": {
                            "line": 10,
                            "column": 12
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 10,
                          "column": 4
                        },
                        "end": {
                          "line": 10,
                          "column": 13
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 9,
                      "column": 22
                    },
                    "end": {
                      "line": 11,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 9,
                    "column": 14
                  },
                  "end": {
                    "line": 11,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 9,
                  "column": 14
                },
                "end": {
                  "line": 11,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 9,
                "column": 1
              },
              "end": {
                "line": 11,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 12,
      "column": 1
    }
  }
}
