{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 10
                },
                "end": {
                  "line": 1,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 10
                    },
                    "end": {
                      "line": 1,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 18
                    },
                    "end": {
                      "line": 1,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "a",
                            "typeAnnotation": {
                              "type": "ETSPrimitiveType",
                              "loc": {
                                "start": {
                                  "line": 2,
                                  "column": 11
                                },
                                "end": {
                                  "line": 2,
                                  "column": 14
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 7
                              },
                              "end": {
                                "line": 2,
                                "column": 8
                              }
                            }
                          },
                          "init": {
                            "type": "NumberLiteral",
                            "value": 1,
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 17
                              },
                              "end": {
                                "line": 2,
                                "column": 18
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 7
                            },
                            "end": {
                              "line": 2,
                              "column": 18
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 3
                        },
                        "end": {
                          "line": 2,
                          "column": 19
                        }
                      }
                    },
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "b",
                            "typeAnnotation": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "Int",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 3,
                                      "column": 11
                                    },
                                    "end": {
                                      "line": 3,
                                      "column": 14
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 3,
                                    "column": 11
                                  },
                                  "end": {
                                    "line": 3,
                                    "column": 16
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 3,
                                  "column": 11
                                },
                                "end": {
                                  "line": 3,
                                  "column": 16
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 7
                              },
                              "end": {
                                "line": 3,
                                "column": 8
                              }
                            }
                          },
                          "init": {
                            "type": "NumberLiteral",
                            "value": 1,
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 17
                              },
                              "end": {
                                "line": 3,
                                "column": 18
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 3,
                              "column": 7
                            },
                            "end": {
                              "line": 3,
                              "column": 18
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 3,
                          "column": 3
                        },
                        "end": {
                          "line": 3,
                          "column": 19
                        }
                      }
                    },
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "c",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 4,
                                "column": 7
                              },
                              "end": {
                                "line": 4,
                                "column": 8
                              }
                            }
                          },
                          "init": {
                            "type": "BinaryExpression",
                            "operator": "===",
                            "left": {
                              "type": "Identifier",
                              "name": "a",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 4,
                                  "column": 11
                                },
                                "end": {
                                  "line": 4,
                                  "column": 12
                                }
                              }
                            },
                            "right": {
                              "type": "Identifier",
                              "name": "b",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 4,
                                  "column": 17
                                },
                                "end": {
                                  "line": 4,
                                  "column": 18
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 4,
                                "column": 11
                              },
                              "end": {
                                "line": 4,
                                "column": 18
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 4,
                              "column": 7
                            },
                            "end": {
                              "line": 4,
                              "column": 18
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 4,
                          "column": 3
                        },
                        "end": {
                          "line": 4,
                          "column": 19
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 23
                    },
                    "end": {
                      "line": 5,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 14
                  },
                  "end": {
                    "line": 5,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 14
                },
                "end": {
                  "line": 5,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 5,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 6,
      "column": 1
    }
  }
}
TypeError: Both operands have to be reference types [referenceEqualityNotReference_n.ets:4:11]
