{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "Foo",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 7
            },
            "end": {
              "line": 1,
              "column": 10
            }
          }
        },
        "typeParameters": {
          "type": "TSTypeParameterDeclaration",
          "params": [
            {
              "type": "TSTypeParameter",
              "name": {
                "type": "Identifier",
                "name": "T",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 11
                  },
                  "end": {
                    "line": 1,
                    "column": 12
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 11
                },
                "end": {
                  "line": 1,
                  "column": 13
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 13
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "resolve",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 12
                },
                "end": {
                  "line": 2,
                  "column": 19
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "resolve",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 12
                    },
                    "end": {
                      "line": 2,
                      "column": 19
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSTypeReference",
                  "part": {
                    "type": "ETSTypeReferencePart",
                    "name": {
                      "type": "Identifier",
                      "name": "Foo",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 26
                        },
                        "end": {
                          "line": 2,
                          "column": 29
                        }
                      }
                    },
                    "typeParams": {
                      "type": "TSTypeParameterInstantiation",
                      "params": [
                        {
                          "type": "ETSTypeReference",
                          "part": {
                            "type": "ETSTypeReferencePart",
                            "name": {
                              "type": "Identifier",
                              "name": "U",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 2,
                                  "column": 30
                                },
                                "end": {
                                  "line": 2,
                                  "column": 31
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 30
                              },
                              "end": {
                                "line": 2,
                                "column": 32
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 30
                            },
                            "end": {
                              "line": 2,
                              "column": 32
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 29
                        },
                        "end": {
                          "line": 2,
                          "column": 32
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 26
                      },
                      "end": {
                        "line": 2,
                        "column": 34
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 26
                    },
                    "end": {
                      "line": 2,
                      "column": 34
                    }
                  }
                },
                "typeParameters": {
                  "type": "TSTypeParameterDeclaration",
                  "params": [
                    {
                      "type": "TSTypeParameter",
                      "name": {
                        "type": "Identifier",
                        "name": "U",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 2,
                            "column": 20
                          },
                          "end": {
                            "line": 2,
                            "column": 21
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 20
                        },
                        "end": {
                          "line": 2,
                          "column": 22
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 19
                    },
                    "end": {
                      "line": 2,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "ETSNewClassInstanceExpression",
                        "typeReference": {
                          "type": "ETSTypeReference",
                          "part": {
                            "type": "ETSTypeReferencePart",
                            "name": {
                              "type": "Identifier",
                              "name": "Foo",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 3,
                                  "column": 20
                                },
                                "end": {
                                  "line": 3,
                                  "column": 23
                                }
                              }
                            },
                            "typeParams": {
                              "type": "TSTypeParameterInstantiation",
                              "params": [
                                {
                                  "type": "ETSTypeReference",
                                  "part": {
                                    "type": "ETSTypeReferencePart",
                                    "name": {
                                      "type": "Identifier",
                                      "name": "U",
                                      "decorators": [],
                                      "loc": {
                                        "start": {
                                          "line": 3,
                                          "column": 24
                                        },
                                        "end": {
                                          "line": 3,
                                          "column": 25
                                        }
                                      }
                                    },
                                    "loc": {
                                      "start": {
                                        "line": 3,
                                        "column": 24
                                      },
                                      "end": {
                                        "line": 3,
                                        "column": 26
                                      }
                                    }
                                  },
                                  "loc": {
                                    "start": {
                                      "line": 3,
                                      "column": 24
                                    },
                                    "end": {
                                      "line": 3,
                                      "column": 26
                                    }
                                  }
                                }
                              ],
                              "loc": {
                                "start": {
                                  "line": 3,
                                  "column": 23
                                },
                                "end": {
                                  "line": 3,
                                  "column": 26
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 20
                              },
                              "end": {
                                "line": 3,
                                "column": 27
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 3,
                              "column": 20
                            },
                            "end": {
                              "line": 3,
                              "column": 27
                            }
                          }
                        },
                        "arguments": [],
                        "loc": {
                          "start": {
                            "line": 3,
                            "column": 16
                          },
                          "end": {
                            "line": 3,
                            "column": 29
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 3,
                          "column": 9
                        },
                        "end": {
                          "line": 3,
                          "column": 29
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 33
                    },
                    "end": {
                      "line": 4,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 19
                  },
                  "end": {
                    "line": 4,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 19
                },
                "end": {
                  "line": 4,
                  "column": 6
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 4,
                "column": 6
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 2
              },
              "end": {
                "line": 5,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 14
          },
          "end": {
            "line": 5,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 6,
      "column": 1
    }
  }
}
