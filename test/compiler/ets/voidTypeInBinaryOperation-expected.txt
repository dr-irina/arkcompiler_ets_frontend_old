{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "check",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 10
                },
                "end": {
                  "line": 1,
                  "column": 15
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "check",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 10
                    },
                    "end": {
                      "line": 1,
                      "column": 15
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 19
                    },
                    "end": {
                      "line": 1,
                      "column": 23
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 24
                    },
                    "end": {
                      "line": 2,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 15
                  },
                  "end": {
                    "line": 2,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 15
                },
                "end": {
                  "line": 2,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 2,
                "column": 2
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 4,
                  "column": 10
                },
                "end": {
                  "line": 4,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 10
                    },
                    "end": {
                      "line": 4,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 18
                    },
                    "end": {
                      "line": 4,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "AssertStatement",
                      "test": {
                        "type": "LogicalExpression",
                        "operator": "||",
                        "left": {
                          "type": "BooleanLiteral",
                          "value": false,
                          "loc": {
                            "start": {
                              "line": 5,
                              "column": 10
                            },
                            "end": {
                              "line": 5,
                              "column": 15
                            }
                          }
                        },
                        "right": {
                          "type": "CallExpression",
                          "callee": {
                            "type": "Identifier",
                            "name": "check",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 5,
                                "column": 19
                              },
                              "end": {
                                "line": 5,
                                "column": 24
                              }
                            }
                          },
                          "arguments": [],
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 5,
                              "column": 19
                            },
                            "end": {
                              "line": 5,
                              "column": 26
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 5,
                            "column": 10
                          },
                          "end": {
                            "line": 5,
                            "column": 26
                          }
                        }
                      },
                      "second": null,
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 3
                        },
                        "end": {
                          "line": 5,
                          "column": 27
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 23
                    },
                    "end": {
                      "line": 6,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 14
                  },
                  "end": {
                    "line": 6,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 4,
                  "column": 14
                },
                "end": {
                  "line": 6,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 4,
                "column": 1
              },
              "end": {
                "line": 6,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 7,
      "column": 1
    }
  }
}
TypeError: Bad operand type, the types of the operands must be boolean type. [voidTypeInBinaryOperation.ets:5:10]
