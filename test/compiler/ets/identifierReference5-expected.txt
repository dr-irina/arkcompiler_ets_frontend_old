{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "B",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 7
            },
            "end": {
              "line": 5,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "b",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 3
                },
                "end": {
                  "line": 6,
                  "column": 4
                }
              }
            },
            "value": {
              "type": "NumberLiteral",
              "value": 2,
              "loc": {
                "start": {
                  "line": 6,
                  "column": 12
                },
                "end": {
                  "line": 6,
                  "column": 13
                }
              }
            },
            "accessibility": "public",
            "static": false,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 6,
                  "column": 6
                },
                "end": {
                  "line": 6,
                  "column": 9
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "c",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 7,
                  "column": 3
                },
                "end": {
                  "line": 7,
                  "column": 4
                }
              }
            },
            "accessibility": "public",
            "static": false,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSFunctionType",
              "params": [
                {
                  "type": "ETSParameterExpression",
                  "name": {
                    "type": "Identifier",
                    "name": "a",
                    "typeAnnotation": {
                      "type": "ETSPrimitiveType",
                      "loc": {
                        "start": {
                          "line": 7,
                          "column": 10
                        },
                        "end": {
                          "line": 7,
                          "column": 13
                        }
                      }
                    },
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 7,
                        "column": 7
                      },
                      "end": {
                        "line": 7,
                        "column": 13
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 7
                    },
                    "end": {
                      "line": 7,
                      "column": 13
                    }
                  }
                }
              ],
              "returnType": {
                "type": "ETSPrimitiveType",
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 18
                  },
                  "end": {
                    "line": 7,
                    "column": 22
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 7,
                  "column": 6
                },
                "end": {
                  "line": 7,
                  "column": 22
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 8,
                "column": 2
              },
              "end": {
                "line": 8,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 5,
            "column": 9
          },
          "end": {
            "line": 8,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 8,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 10,
              "column": 7
            },
            "end": {
              "line": 10,
              "column": 8
            }
          }
        },
        "superClass": {
          "type": "ETSTypeReference",
          "part": {
            "type": "ETSTypeReferencePart",
            "name": {
              "type": "Identifier",
              "name": "B",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 10,
                  "column": 17
                },
                "end": {
                  "line": 10,
                  "column": 18
                }
              }
            },
            "loc": {
              "start": {
                "line": 10,
                "column": 17
              },
              "end": {
                "line": 10,
                "column": 20
              }
            }
          },
          "loc": {
            "start": {
              "line": 10,
              "column": 17
            },
            "end": {
              "line": 10,
              "column": 20
            }
          }
        },
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 11,
                  "column": 3
                },
                "end": {
                  "line": 11,
                  "column": 6
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 11,
                      "column": 3
                    },
                    "end": {
                      "line": 11,
                      "column": 6
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 11,
                      "column": 10
                    },
                    "end": {
                      "line": 11,
                      "column": 14
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "AssignmentExpression",
                        "operator": "=",
                        "left": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "ThisExpression",
                            "loc": {
                              "start": {
                                "line": 12,
                                "column": 5
                              },
                              "end": {
                                "line": 12,
                                "column": 9
                              }
                            }
                          },
                          "property": {
                            "type": "Identifier",
                            "name": "b",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 12,
                                "column": 10
                              },
                              "end": {
                                "line": 12,
                                "column": 11
                              }
                            }
                          },
                          "computed": false,
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 12,
                              "column": 5
                            },
                            "end": {
                              "line": 12,
                              "column": 11
                            }
                          }
                        },
                        "right": {
                          "type": "NumberLiteral",
                          "value": 3,
                          "loc": {
                            "start": {
                              "line": 12,
                              "column": 14
                            },
                            "end": {
                              "line": 12,
                              "column": 15
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 12,
                            "column": 5
                          },
                          "end": {
                            "line": 12,
                            "column": 15
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 12,
                          "column": 5
                        },
                        "end": {
                          "line": 12,
                          "column": 16
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 11,
                      "column": 15
                    },
                    "end": {
                      "line": 13,
                      "column": 4
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 11,
                    "column": 6
                  },
                  "end": {
                    "line": 13,
                    "column": 4
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 11,
                  "column": 6
                },
                "end": {
                  "line": 13,
                  "column": 4
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 11,
                "column": 3
              },
              "end": {
                "line": 13,
                "column": 4
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 15,
                  "column": 10
                },
                "end": {
                  "line": 15,
                  "column": 13
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 15,
                      "column": 10
                    },
                    "end": {
                      "line": 15,
                      "column": 13
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 15,
                      "column": 17
                    },
                    "end": {
                      "line": 15,
                      "column": 21
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "AssignmentExpression",
                        "operator": "=",
                        "left": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "ThisExpression",
                            "loc": {
                              "start": {
                                "line": 16,
                                "column": 5
                              },
                              "end": {
                                "line": 16,
                                "column": 9
                              }
                            }
                          },
                          "property": {
                            "type": "Identifier",
                            "name": "c",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 16,
                                "column": 10
                              },
                              "end": {
                                "line": 16,
                                "column": 11
                              }
                            }
                          },
                          "computed": false,
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 16,
                              "column": 5
                            },
                            "end": {
                              "line": 16,
                              "column": 11
                            }
                          }
                        },
                        "right": {
                          "type": "Identifier",
                          "name": "bar",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 16,
                              "column": 14
                            },
                            "end": {
                              "line": 16,
                              "column": 17
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 16,
                            "column": 5
                          },
                          "end": {
                            "line": 16,
                            "column": 17
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 16,
                          "column": 5
                        },
                        "end": {
                          "line": 16,
                          "column": 18
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 15,
                      "column": 22
                    },
                    "end": {
                      "line": 17,
                      "column": 4
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 15,
                    "column": 13
                  },
                  "end": {
                    "line": 17,
                    "column": 4
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 15,
                  "column": 13
                },
                "end": {
                  "line": 17,
                  "column": 4
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 15,
                "column": 3
              },
              "end": {
                "line": 17,
                "column": 4
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 18,
                "column": 2
              },
              "end": {
                "line": 18,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 10,
            "column": 19
          },
          "end": {
            "line": 18,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 10,
          "column": 1
        },
        "end": {
          "line": 18,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "bar",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 10
                },
                "end": {
                  "line": 1,
                  "column": 13
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "bar",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 10
                    },
                    "end": {
                      "line": 1,
                      "column": 13
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "ETSPrimitiveType",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 17
                          },
                          "end": {
                            "line": 1,
                            "column": 20
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 14
                        },
                        "end": {
                          "line": 1,
                          "column": 20
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 14
                      },
                      "end": {
                        "line": 1,
                        "column": 20
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 23
                    },
                    "end": {
                      "line": 1,
                      "column": 27
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 28
                    },
                    "end": {
                      "line": 3,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 13
                  },
                  "end": {
                    "line": 3,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 13
                },
                "end": {
                  "line": 3,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 3,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 19,
      "column": 1
    }
  }
}
TypeError: 'this' cannot be referenced from a static context [identifierReference5.ets:16:5]
