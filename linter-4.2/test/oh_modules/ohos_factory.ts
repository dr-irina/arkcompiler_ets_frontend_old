export declare class Something { beep(): number }

export declare class SomethingFactory {
    private constructor();

    public static getInstance(): SomethingFactory;

    public create<T extends Something>(smth: { new(): T }): T;
}

export declare class SomethingBar extends Something {}
