/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// 3D Cube Rotation
// http://www.speich.net/computer/moztesting/3d.htm
// Created by Simon Speich

function Cube3D() {
  this.Q = new Array();
  this.MTrans = new Array();  // transformation matrix
  this.MQube = new Array();   // position information of qube
  this.I = new Array();       // entity matrix
  this.Origin = new Object();
  this.Testing = new Object();
  this.LoopTimer;
  this.DisplArea = new Object();
  this.DisplArea.Width = 300;
  this.DisplArea.Height = 300;

  this.validation = {
    20: 2889.0000000000045,
    40: 2889.0000000000055,
    80: 2889.000000000005,
    160: 2889.0000000000055,
  };
}

Cube3D.prototype.DrawLine = function(From, To) {
  let x1 = From.V[0];
  let x2 = To.V[0];
  let y1 = From.V[1];
  let y2 = To.V[1];
  let dx = Math.abs(x2 - x1);
  let dy = Math.abs(y2 - y1);
  let x = x1;
  let y = y1;
  let IncX1, IncY1;
  let IncX2, IncY2;
  let Den;
  let Num;
  let NumAdd;
  let NumPix;

  if (x2 >= x1) {
    IncX1 = 1;
    IncX2 = 1;
  } else {
    IncX1 = -1;
    IncX2 = -1;
  }
  if (y2 >= y1) {
    IncY1 = 1;
    IncY2 = 1;
  } else {
    IncY1 = -1;
    IncY2 = -1;
  }

  if (dx >= dy) {
    IncX1 = 0;
    IncY2 = 0;
    Den = dx;
    Num = dx / 2;
    NumAdd = dy;
    NumPix = dx;
  } else {
    IncX2 = 0;
    IncY1 = 0;
    Den = dy;
    Num = dy / 2;
    NumAdd = dx;
    NumPix = dy;
  }

  NumPix = Math.round(this.Q.LastPx + NumPix);

  let i = this.Q.LastPx;
  for (; i < NumPix; i++) {
    Num += NumAdd;
    if (Num >= Den) {
      Num -= Den;
      x += IncX1;
      y += IncY1;
    }
    x += IncX2;
    y += IncY2;
  }
  this.Q.LastPx = NumPix;
};

Cube3D.prototype.CalcCross = function(V0, V1) {
  let Cross = new Array();
  Cross[0] = V0[1] * V1[2] - V0[2] * V1[1];
  Cross[1] = V0[2] * V1[0] - V0[0] * V1[2];
  Cross[2] = V0[0] * V1[1] - V0[1] * V1[0];
  return Cross;
};

Cube3D.prototype.CalcNormal = function(V0, V1, V2) {
  let A = new Array();
  let B = new Array();
  for (let i = 0; i < 3; i++) {
    A[i] = V0[i] - V1[i];
    B[i] = V2[i] - V1[i];
  }
  A = this.CalcCross(A, B);
  let Length = Math.sqrt(A[0] * A[0] + A[1] * A[1] + A[2] * A[2]);
  for (let i = 0; i < 3; i++) A[i] = A[i] / Length;
  A[3] = 1;
  return A;
};

Cube3D.prototype.CreateP = function(X, Y, Z) {
  this.V = [X, Y, Z, 1];
};

// multiplies two matrices
Cube3D.prototype.MMulti = function(M1, M2) {
  let M = [[], [], [], []];
  let i = 0;
  let j = 0;
  for (; i < 4; i++) {
    j = 0;
    for (; j < 4; j++)
      M[i][j] = M1[i][0] * M2[0][j] + M1[i][1] * M2[1][j] +
          M1[i][2] * M2[2][j] + M1[i][3] * M2[3][j];
  }
  return M;
};

// multiplies matrix with vector
Cube3D.prototype.VMulti = function(M, V) {
  let Vect = new Array();
  let i = 0;
  for (; i < 4; i++)
    Vect[i] = M[i][0] * V[0] + M[i][1] * V[1] + M[i][2] * V[2] + M[i][3] * V[3];
  return Vect;
};

Cube3D.prototype.VMulti2 = function(M, V) {
  let Vect = new Array();
  let i = 0;
  for (; i < 3; i++) Vect[i] = M[i][0] * V[0] + M[i][1] * V[1] + M[i][2] * V[2];
  return Vect;
};

// add to matrices
Cube3D.prototype.MAdd = function(M1, M2) {
  let M = [[], [], [], []];
  let i = 0;
  let j = 0;
  for (; i < 4; i++) {
    j = 0;
    for (; j < 4; j++) M[i][j] = M1[i][j] + M2[i][j];
  }
  return M;
};

Cube3D.prototype.Translate = function(M, Dx, Dy, Dz) {
  let T = [
    [1, 0, 0, Dx],
    [0, 1, 0, Dy],
    [0, 0, 1, Dz],
    [0, 0, 0, 1],
  ];
  return this.MMulti(T, M);
};

Cube3D.prototype.RotateX = function(M, Phi) {
  let a = Phi;
  a *= Math.PI / 180;
  let Cos = Math.cos(a);
  let Sin = Math.sin(a);
  let R = [
    [1, 0, 0, 0],
    [0, Cos, -Sin, 0],
    [0, Sin, Cos, 0],
    [0, 0, 0, 1],
  ];
  return this.MMulti(R, M);
};

Cube3D.prototype.RotateY = function(M, Phi) {
  let a = Phi;
  a *= Math.PI / 180;
  let Cos = Math.cos(a);
  let Sin = Math.sin(a);
  let R = [
    [Cos, 0, Sin, 0],
    [0, 1, 0, 0],
    [-Sin, 0, Cos, 0],
    [0, 0, 0, 1],
  ];
  return this.MMulti(R, M);
};

Cube3D.prototype.RotateZ = function(M, Phi) {
  let a = Phi;
  a *= Math.PI / 180;
  let Cos = Math.cos(a);
  let Sin = Math.sin(a);
  let R = [
    [Cos, -Sin, 0, 0],
    [Sin, Cos, 0, 0],
    [0, 0, 1, 0],
    [0, 0, 0, 1],
  ];
  return this.MMulti(R, M);
};

Cube3D.prototype.DrawQube = function() {
  // calc current normals
  let CurN = new Array();
  let i = 5;
  this.Q.LastPx = 0;
  for (; i > -1; i--) CurN[i] = this.VMulti2(this.MQube, this.Q.Normal[i]);
  if (CurN[0][2] < 0) {
    if (!this.Q.Line[0]) {
      this.DrawLine(this.Q[0], this.Q[1]);
      this.Q.Line[0] = true;
    };
    if (!this.Q.Line[1]) {
      this.DrawLine(this.Q[1], this.Q[2]);
      this.Q.Line[1] = true;
    };
    if (!this.Q.Line[2]) {
      this.DrawLine(this.Q[2], this.Q[3]);
      this.Q.Line[2] = true;
    };
    if (!this.Q.Line[3]) {
      this.DrawLine(this.Q[3], this.Q[0]);
      this.Q.Line[3] = true;
    };
  }
  if (CurN[1][2] < 0) {
    if (!this.Q.Line[2]) {
      this.DrawLine(this.Q[3], this.Q[2]);
      this.Q.Line[2] = true;
    };
    if (!this.Q.Line[9]) {
      this.DrawLine(this.Q[2], this.Q[6]);
      this.Q.Line[9] = true;
    };
    if (!this.Q.Line[6]) {
      this.DrawLine(this.Q[6], this.Q[7]);
      this.Q.Line[6] = true;
    };
    if (!this.Q.Line[10]) {
      this.DrawLine(this.Q[7], this.Q[3]);
      this.Q.Line[10] = true;
    };
  }
  if (CurN[2][2] < 0) {
    if (!this.Q.Line[4]) {
      this.DrawLine(this.Q[4], this.Q[5]);
      this.Q.Line[4] = true;
    };
    if (!this.Q.Line[5]) {
      this.DrawLine(this.Q[5], this.Q[6]);
      this.Q.Line[5] = true;
    };
    if (!this.Q.Line[6]) {
      this.DrawLine(this.Q[6], this.Q[7]);
      this.Q.Line[6] = true;
    };
    if (!this.Q.Line[7]) {
      this.DrawLine(this.Q[7], this.Q[4]);
      this.Q.Line[7] = true;
    };
  }
  if (CurN[3][2] < 0) {
    if (!this.Q.Line[4]) {
      this.DrawLine(this.Q[4], this.Q[5]);
      this.Q.Line[4] = true;
    };
    if (!this.Q.Line[8]) {
      this.DrawLine(this.Q[5], this.Q[1]);
      this.Q.Line[8] = true;
    };
    if (!this.Q.Line[0]) {
      this.DrawLine(this.Q[1], this.Q[0]);
      this.Q.Line[0] = true;
    };
    if (!this.Q.Line[11]) {
      this.DrawLine(this.Q[0], this.Q[4]);
      this.Q.Line[11] = true;
    };
  }
  if (CurN[4][2] < 0) {
    if (!this.Q.Line[11]) {
      this.DrawLine(this.Q[4], this.Q[0]);
      this.Q.Line[11] = true;
    };
    if (!this.Q.Line[3]) {
      this.DrawLine(this.Q[0], this.Q[3]);
      this.Q.Line[3] = true;
    };
    if (!this.Q.Line[10]) {
      this.DrawLine(this.Q[3], this.Q[7]);
      this.Q.Line[10] = true;
    };
    if (!this.Q.Line[7]) {
      this.DrawLine(this.Q[7], this.Q[4]);
      this.Q.Line[7] = true;
    };
  }
  if (CurN[5][2] < 0) {
    if (!this.Q.Line[8]) {
      this.DrawLine(this.Q[1], this.Q[5]);
      this.Q.Line[8] = true;
    };
    if (!this.Q.Line[5]) {
      this.DrawLine(this.Q[5], this.Q[6]);
      this.Q.Line[5] = true;
    };
    if (!this.Q.Line[9]) {
      this.DrawLine(this.Q[6], this.Q[2]);
      this.Q.Line[9] = true;
    };
    if (!this.Q.Line[1]) {
      this.DrawLine(this.Q[2], this.Q[1]);
      this.Q.Line[1] = true;
    };
  }
  this.Q.Line = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  this.Q.LastPx = 0;
};

Cube3D.prototype.Loop = function() {
  if (this.Testing.LoopCount > this.Testing.LoopMax) return;
  let TestingStr = String(this.Testing.LoopCount);
  while (TestingStr.length < 3) TestingStr = '0' + TestingStr;
  this.MTrans =
      this.Translate(I, -this.Q[8].V[0], -this.Q[8].V[1], -this.Q[8].V[2]);
  this.MTrans = this.RotateX(this.MTrans, 1);
  this.MTrans = this.RotateY(this.MTrans, 3);
  this.MTrans = this.RotateZ(this.MTrans, 5);
  this.MTrans = this.Translate(
      this.MTrans, this.Q[8].V[0], this.Q[8].V[1], this.Q[8].V[2]);
  this.MQube = this.MMulti(this.MTrans, this.MQube);
  let i = 8;
  for (; i > -1; i--) {
    this.Q[i].V = this.VMulti(this.MTrans, this.Q[i].V);
  }
  this.DrawQube();
  this.Testing.LoopCount++;
  this.Loop();
};

Cube3D.prototype.Init = function(CubeSize) {
  // init/reset vars
  this.Origin.V = [150, 150, 20, 1];
  this.Testing.LoopCount = 0;
  this.Testing.LoopMax = 50;
  this.Testing.TimeMax = 0;
  this.Testing.TimeAvg = 0;
  this.Testing.TimeMin = 0;
  this.Testing.TimeTemp = 0;
  this.Testing.TimeTotal = 0;
  this.Testing.Init = false;

  // transformation matrix
  this.MTrans = [
    [1, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, 0],
    [0, 0, 0, 1],
  ];

  // position information of qube
  this.MQube = [
    [1, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, 0],
    [0, 0, 0, 1],
  ];

  // entity matrix
  let I = [
    [1, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, 0],
    [0, 0, 0, 1],
  ];

  // create qube
  this.Q[0] = new this.CreateP(-CubeSize, -CubeSize, CubeSize);
  this.Q[1] = new this.CreateP(-CubeSize, CubeSize, CubeSize);
  this.Q[2] = new this.CreateP(CubeSize, CubeSize, CubeSize);
  this.Q[3] = new this.CreateP(CubeSize, -CubeSize, CubeSize);
  this.Q[4] = new this.CreateP(-CubeSize, -CubeSize, -CubeSize);
  this.Q[5] = new this.CreateP(-CubeSize, CubeSize, -CubeSize);
  this.Q[6] = new this.CreateP(CubeSize, CubeSize, -CubeSize);
  this.Q[7] = new this.CreateP(CubeSize, -CubeSize, -CubeSize);

  // center of gravity
  this.Q[8] = new this.CreateP(0, 0, 0);

  // anti-clockwise edge check
  this.Q.Edge = [
    [0, 1, 2],
    [3, 2, 6],
    [7, 6, 5],
    [4, 5, 1],
    [4, 0, 3],
    [1, 5, 6],
  ];

  // calculate squad normals
  this.Q.Normal = new Array();
  for (let i = 0; i < this.Q.Edge.length; i++)
    this.Q.Normal[i] = this.CalcNormal(
        this.Q[this.Q.Edge[i][0]].V, this.Q[this.Q.Edge[i][1]].V,
        this.Q[this.Q.Edge[i][2]].V);

  // line drawn ?
  this.Q.Line = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];

  // create line pixels
  this.Q.NumPx = 9 * 2 * CubeSize;
  for (let i = 0; i < this.Q.NumPx; i++) this.CreateP(0, 0, 0);

  this.MTrans = this.Translate(
      this.MTrans, this.Origin.V[0], this.Origin.V[1], this.Origin.V[2]);
  this.MQube = this.MMulti(this.MTrans, this.MQube);

  let i = 0;
  for (; i < 9; i++) {
    this.Q[i].V = this.VMulti(this.MTrans, this.Q[i].V);
  }
  this.DrawQube();
  this.Testing.Init = true;
  this.Loop();

  // Perform a simple sum-based verification.
  let sum = 0;
  for (let i = 0; i < this.Q.length; ++i) {
    let vector = this.Q[i].V;
    for (let j = 0; j < vector.length; ++j) sum += vector[j];
  }
  if (sum != this.validation[CubeSize])
    throw 'Error: bad vector sum for CubeSize = ' + CubeSize + '; expected ' +
        this.validation[CubeSize] + ' but got ' + sum;
};

Cube3D.prototype.doTest = function() {
  for (let i = 20; i <= 160; i *= 2) {
    this.Init(i);
  }
};

Cube3D.prototype.setup = function() {};

Cube3D.prototype.run = function() {
  this.doTest();

  let consumer = new consumer();
  consumer.consumeInt(this.result);
};