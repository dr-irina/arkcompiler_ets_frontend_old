/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
//
// contributed by Ian Osgood

function A(i, j) {
  return 1 / ((i + j) * (i + j + 1) / 2 + i + 1);
}

function Au(u, v) {
  for (let i: int = 0; i < u.length; ++i) {
    let t: int = 0;
    for (let j: int = 0; j < u.length; ++j) t += A(i, j) * u[j];
    v[i] = t;
  }
}

function Atu(u, v) {
  for (let i: int = 0; i < u.length; ++i) {
    let t: int = 0;
    for (let j: int = 0; j < u.length; ++j) t += A(j, i) * u[j];
    v[i] = t;
  }
}

function AtAu(u, v, w) {
  Au(u, w);
  Atu(w, v);
}

function MathSpectralNorm() {
  this.n1 = 6;
  this.n2 = 48;
}

MathSpectralNorm.prototype.spectralnorm = function(n) {
  let i: int;
  let u: int[] = [];
  let v: int[] = [];
  let w: int[] = [];
  let vv: int = 0;
  let vBv: int = 0;

  for (i = 0; i < n; ++i) {
    u[i] = 1;
    v[i] = w[i] = 0;
  }
  for (i = 0; i < 10; ++i) {
    AtAu(u, v, w);
    AtAu(v, u, w);
  }
  for (i = 0; i < n; ++i) {
    vBv += u[i] * v[i];
    vv += v[i] * v[i];
  }
  return Math.sqrt(vBv / vv);
};

MathSpectralNorm.prototype.setup = function() {};

MathSpectralNorm.prototype.run = function() {
  let total: double = 0;

  let expected: double = 5.086694231303284;

  for (let i: int = this.n1; i <= this.n2; i *= 2) {
    total += this.spectralnorm(i);
  }

  if (total != expected)
    throw 'ERROR: bad result: expected ' + expected + ' but got ' + total;

  let consumer = new consumer();
  consumer.consumeFloat(total);
};
