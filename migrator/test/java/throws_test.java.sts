/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ohos.migrator.tests.java;

import * from "std/math/math";
open class MyException extends Exception  {
    constructor() {
    }

    constructor(s : String) {
        super(s);
    }

}

open class AnotherException extends Exception  {
}

open class SuperClass  {
    private s : String ;
    public constructor(s : String) throws {
        if (s == null) throw new MyException();
        this.s = s;
    }

    open foo(): void {
        System.out.println(s);
    }
}

open class SubClass1 extends SuperClass  {
    public constructor(s : String) throws {
        super(s);
        if (s == null) throw new AnotherException();
    }

    override foo(): void {
        throw new UnsupportedOperationException();
    }
    public open run(): void {
        foo();
    }
    public open toto(): void  throws {
        throw new MyException();
    }
}

open class SubClass2 extends SubClass1  {
    public constructor(s : String) throws {
        super(s);
        if (s == "!") throw new UnsupportedOperationException();
    }

    public override foo(): void {
        super.foo();
    }
    public open bar(): void  throws {
        super.foo();
        throw new AnotherException();
    }
    public override toto(): void  throws {
        super.toto();
    }
}

open class Test  {
// throws
    public static test1(s : String): void  throws {
        let sc1 : SubClass1 = new SubClass1(s);
        sc1.foo();
    }
// throws
    public static test2(s : String): void  throws {
        try {
            let sc1 : SubClass1 = new SubClass1(s);
        }
        catch (e : MyException) {
        }

    }
// no throws - all exceptions are catched
    public static test3(s : String): void {
        try {
            let sc1 : SubClass1 = new SubClass1(s);
        }
        catch (e : MyException) {
        }
        catch (e : AnotherException) {
        }

    }
// no throws - only runtime exceptions 
    public static test4(s : String): void {
        try {
            let sc2 : SubClass2 = new SubClass2(s);
        }
        catch (e : MyException) {
        }
        catch (e : AnotherException) {
        }

    }
// no throws - only runtime exceptions
    public static test5(s : String): void {
        try {
            let sc2 : SubClass2 = new SubClass2(s);
            sc2.foo();
        }
        catch (e : MyException) {
        }
        catch (e : AnotherException) {
        }

    }
// throws
    public static test6(s : String): void  throws {
        try {
            let sc2 : SubClass2 = new SubClass2(s);
            sc2.bar();
        }
        catch (e : MyException) {
        }

    }
// test for constructor with static initializer
    open class E1 extends Exception  {
    }

    open class E2 extends Exception  {
    }

    open class E3 extends Exception  {
    }

    abstract class Ta  {
        public abstract m(): void  throws ;
    }

    interface Tb {
        m(): void  throws ;
    }

// intersect throws clauses
    abstract class Tc extends Ta implements Tb  {
        public constructor() {
            try {
                m();
            }
            catch (e2 : E2) {
            }

        }

    }

// STS constructor must have throws clause 
    abstract class Td extends Ta  {
        public constructor() throws {
            try {
                m();
            }
            catch (e2 : E2) {
            }

        }

    }

    interface FunctionalInterface {
        foo(): void ;
    }

    abstract inner class AbstractException extends Error implements FunctionalInterface  {
    }

    readonly notInitialized : AbstractException = (((): void => {
        throw notInitialized;
    }
) as FunctionalInterface) as AbstractException;
}

